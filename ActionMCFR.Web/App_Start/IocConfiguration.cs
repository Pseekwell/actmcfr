﻿using System.Web.Http;
using ActionMCFR.Data;
using ActionMCFR.DataContracts;
using ActionMCFR.Model;
using ActionMCFR.DataInfrostructure;
using Ninject;

namespace ActionMCFR.Web
{
    public class IocConfiguration
    {
        public static void Register(HttpConfiguration config)
        {
            var kernel = new StandardKernel();

            kernel.Bind<MemoryRepository<TaxPayment>>().To<MemoryRepository<TaxPayment>>()
                .InSingletonScope();

            kernel.Bind<IRepository<TaxPayment>>().To<TaxPaymentsMemRepository>()
                .InSingletonScope();

            kernel.Bind<TaxService>().ToSelf().InSingletonScope();
            kernel.Bind<ITaxService>().ToMethod(context => context.Kernel.Get<TaxService>());


            config.DependencyResolver = new NinjectDependencyResolver(kernel);
        }
    }
}