﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ActionMCFR.DataInfrostructure;
using ActionMCFR.Model;

namespace ActionMCFR.Web.Controllers
{
    public class EasyTaxController : ApiBaseController
    {
        public EasyTaxController(ITaxService taxService)
        {
            TaxService = taxService;
        }

        public List<string> Get(string inn)
        {
            List<string> results = TaxService.GetTaxRegimeEasy(inn);

            if (results != null)
                return results;

            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
        }
    }
}
