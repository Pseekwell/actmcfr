﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ActionMCFR.DataInfrostructure;


namespace ActionMCFR.Web.Controllers
{
    public abstract class ApiBaseController : ApiController
    {
        protected ITaxService TaxService { get; set; }
    }
}
