﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ActionMCFR.Model
{
    [Table("FnsTaxRegime")]
    public class FnsTaxRegime
    {
        [Key]
        public string Inn { get; set; }
        public bool IsSrp { get; set; }
        public bool IsEnvd { get; set; }
        public bool IsUsn { get; set; }
        public bool IsEshn { get; set; }

        [NotMapped]
        public List<string> FullTaxName
        {
            get {
                List<string> result = new List<string>();

                if (this.IsSrp)
                    result.Add("СРП");

                if(this.IsEnvd)
                    result.Add("ЕНВД");

                if (this.IsUsn)
                    result.Add("УСН");

                if (this.IsEshn)
                    result.Add("ЕСХН");

                return result; }
        }
    }
}
