﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionMCFR.Model
{
    public class TaxPayment
    {
        public string Inn { get; set; }
        public string TaxName { get; set; }
        public double Amount { get; set; }
    }
}
