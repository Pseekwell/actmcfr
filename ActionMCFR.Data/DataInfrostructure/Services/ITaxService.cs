﻿using System.Collections.Generic;
using ActionMCFR.Model;

namespace ActionMCFR.DataInfrostructure
{
    public interface ITaxService
    {
        List<string> GetTaxRegimeEasy(string Inn);
        List<string> GetTaxRegimeExtended(string Inn);
    }
}
