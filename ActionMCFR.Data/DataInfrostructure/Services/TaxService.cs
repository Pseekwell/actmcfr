﻿using System.Collections.Generic;
using ActionMCFR.Model;
using ActionMCFR.DataContracts;
using ActionMCFR.Data;
using System.Linq;

namespace ActionMCFR.DataInfrostructure
{
    public class TaxService : ITaxService
    {
        readonly IRepository<TaxPayment> _TaxPayments;
        readonly IRepository<FnsTaxRegime> _FnsTaxRegimes;

        //TODO: Add FnsTaxRegimeEFRepository to IocConfiguration, Remove from Constructor with its ActionDbContext
        public TaxService(IRepository<TaxPayment> TaxPayments)
        {
            this._TaxPayments = TaxPayments;

            ActionDbContext dbContext = new ActionDbContext();
            this._FnsTaxRegimes = new FnsTaxRegimeEFRepository(dbContext);
        }

        public List<string> GetTaxRegimeEasy(string Inn)
        {
            List<string> Result = new List<string>();

            InnHasRegimeSpec ihrSpec = new InnHasRegimeSpec(Inn);
            List<FnsTaxRegime> res = _FnsTaxRegimes.Get(ihrSpec).ToList();

            if (res != null && res.Count > 0)
                Result = res[0].FullTaxName;
            else
                Result.Add("ОСН");

            return Result;
        }

        public List<string> GetTaxRegimeExtended(string Inn)
        {
            List<string> Result = this.GetTaxRegimeEasy(Inn);

            if (Result.Contains("ЕНВД"))
            {
                InnHasTaxPayedSpec tpSpec = new InnHasTaxPayedSpec(Inn, "налог на прибыль");
                if (_TaxPayments.Get(tpSpec).Count()>0)
                {
                    return new List<string>(new string[] { "ОСН", "ЕНВД" });
                }
            }

            return Result;
        }
    }
}
