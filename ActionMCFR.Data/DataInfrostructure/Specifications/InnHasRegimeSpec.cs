﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using ActionMCFR.Model;

namespace ActionMCFR.DataInfrostructure
{
    public class InnHasRegimeSpec : BaseSpecification<FnsTaxRegime>
    {
        readonly string _inn;

        public InnHasRegimeSpec(string Inn)
        {
            this._inn = Inn;
        }

        public override Expression<Func<FnsTaxRegime, bool>> SpecExpression
        {
            get
            {
                return FnsTaxRegime => FnsTaxRegime.Inn == _inn &&
                 (FnsTaxRegime.IsEnvd || FnsTaxRegime.IsEshn || FnsTaxRegime.IsSrp || FnsTaxRegime.IsUsn);
            }
        }
    }
}
