﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using ActionMCFR.Model;

namespace ActionMCFR.DataInfrostructure
{
    public class InnHasTaxPayedSpec : BaseSpecification<TaxPayment>
    {
        readonly string _taxName;
        readonly string _inn;

        public InnHasTaxPayedSpec(string Inn, string TaxName)
        {
            this._taxName = TaxName;
            this._inn = Inn;
        }

        public override Expression<Func<TaxPayment, bool>> SpecExpression
        {
            get
            {
                return TaxPayment => 
                           TaxPayment.Inn == _inn && 
                           TaxPayment.TaxName == _taxName && 
                           TaxPayment.Amount > 0;
            }
        }
    }
}
