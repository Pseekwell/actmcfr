﻿using System.Collections.Generic;
using ActionMCFR.DataInfrostructure;

namespace ActionMCFR.DataContracts
{
    public interface IRepository<T>
    {
        IEnumerable<T> Get(ISpecification<T> spec);
        void Add(T entity);
    }
}
