﻿using System.Data.Entity;
using ActionMCFR.Model;

namespace ActionMCFR.Data
{
    public class FnsTaxRegimeEFRepository : EFRepository<FnsTaxRegime>
    {
        public FnsTaxRegimeEFRepository(DbContext context) : base(context) { }
    }
}
