﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ActionMCFR.DataContracts;
using System.Data.Entity;
using ActionMCFR.DataInfrostructure;

namespace ActionMCFR.Data
{
    public class EFRepository<T> : IRepository<T> where T : class
    {
        public EFRepository(DbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }

        protected DbSet<T> DbSet { get; set; }

        public void Add(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<T> Get(ISpecification<T> spec)
        {
            return DbSet.AsNoTracking().Where(spec.SpecExpression).ToList();
        }
    }
}

