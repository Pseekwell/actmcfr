﻿using System.Data.Entity;
using ActionMCFR.Model;

namespace ActionMCFR.Data
{
    public class ActionDbContext : DbContext
    {
        public ActionDbContext()
           : base("ActionConnectionString") { }

        public DbSet<FnsTaxRegime> FnsTaxRegimes { get; set; }
    }
}
