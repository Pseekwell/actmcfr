﻿using System.Collections.Generic;
using System.Linq;
using ActionMCFR.DataContracts;
using ActionMCFR.DataInfrostructure;

namespace ActionMCFR.Data 
{
    public class MemoryRepository<T> : IRepository<T>
    {
        protected static List<T> entities = new List<T>();

        public IEnumerable<T> Get(ISpecification<T> spec) => entities.Where(spec.IsSatisfiedBy);

        public void Add(T entity) => entities.Add(entity);
    }
}
