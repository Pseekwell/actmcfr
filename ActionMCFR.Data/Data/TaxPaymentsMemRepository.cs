﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ActionMCFR.Model;
using ActionMCFR.DataContracts;
using ActionMCFR.DataInfrostructure;

namespace ActionMCFR.Data
{
    public class TaxPaymentsMemRepository : IRepository<TaxPayment>
    {
        readonly MemoryRepository<TaxPayment> _memRepository;

        public TaxPaymentsMemRepository(MemoryRepository<TaxPayment> memRepository)
        {
            this._memRepository = memRepository;

            _memRepository.Add(new TaxPayment { Inn = "1111111111", TaxName = "налог на прибыль", Amount = 1234.12 });
        }

        public void Add(TaxPayment entity) => this._memRepository.Add(entity);

        public IEnumerable<TaxPayment> Get(ISpecification<TaxPayment> spec) => this._memRepository.Get(spec);
    }
}
