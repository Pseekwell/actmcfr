# Project Title

Тестовое задание Актион-МЦФЭР от Сухорученко Евгения

## Getting Started

Решение содержит 3 проекта. Данные, модели, WEB API.
Для запуска поменять ConnectionString WEB API

## Running the tests
http://localhost/api/EasyTax/1111111111
http://localhost/api/ExtendedTax/1111111111

http://localhost/api/EasyTax/2222222222
http://localhost/api/ExtendedTax/2222222222

routeTemplate: "api/{controller}/{inn}"

### Prerequisites

Ninject+Ninject.Common+Ninject.MVC5, EntityFramework6 
(restore Nuget Packeges from VisualStudio)

## Built With
Specification Pattern,
Repository Pattern,
AppService as EntryPoint

### Installing
БД не прикладываю, по опыту, разные версии SQL итд...

CREATE TABLE [dbo].[FnsTaxRegime](
[Ogrn] [varchar](15) NULL,
[Inn] [varchar](12) NOT NULL,
Евгений Михайлович [nvarchar](1024) NULL,
[IsSrp] [bit] NULL,
[IsEnvd] [bit] NULL,
[IsUsn] [bit] NULL,
[IsEshn] [bit] NULL,
[DocId] [uniqueidentifier] NULL,
[DocDate] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
[Inn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Режим - Соглашение о разделе продукции ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FnsTaxRegime', @level2type=N'COLUMN',@level2name=N'IsSrp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Режим - Единый налог на вмененный доход' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FnsTaxRegime', @level2type=N'COLUMN',@level2name=N'IsEnvd'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Режим - Упрощенная система налогообложения' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FnsTaxRegime', @level2type=N'COLUMN',@level2name=N'IsUsn'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Режим - Единый сельскохозяйственный налог' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FnsTaxRegime', @level2type=N'COLUMN',@level2name=N'IsEshn'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Данные о специальных налоговых режимах от ФНС (https://www.nalog.ru/opendata/7707329152-snr)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FnsTaxRegime'
GO

INSERT INTO [dbo].[FnsTaxRegime]
           ([Ogrn],[Inn],[IsSrp],[IsEnvd],[IsUsn],[IsEshn],[DocId],[DocDate])
     VALUES('1111111111','1111111111',1,1,1,1,NEWID(),GETDATE())
GO

INSERT INTO [dbo].[FnsTaxRegime]
           ([Ogrn],[Inn],[IsSrp],[IsEnvd],[IsUsn],[IsEshn],[DocId],[DocDate])
     VALUES('2222222222','2222222222',1,1,0,0,NEWID(),GETDATE())
GO
